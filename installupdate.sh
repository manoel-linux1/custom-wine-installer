#!/bin/bash

clear

show_main_menu() {
while true; do
clear
echo "#################################################################"
echo "(custom-wine-installer) >> (feb 2024)"
echo "#################################################################"
echo "██     ██ ██ ███    ██ ███████ "
echo "██     ██ ██ ████   ██ ██      "
echo "██  █  ██ ██ ██ ██  ██ █████   "
echo "██ ███ ██ ██ ██  ██ ██ ██      "
echo " ███ ███  ██ ██   ████ ███████ "
echo "#################################################################"
echo "(custom-wine-installer-gitlab) >> (https://gitlab.com/manoel-linux1/custom-wine-installer)"
echo "#################################################################"

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

sudo pacman -Sy
sudo pacman -S iputils -y
echo "#################################################################"
sudo xbps-install -Sy
sudo xbps-install -S inetutils-ping -y
echo "#################################################################"

wine-ge='https://github.com/GloriousEggroll/wine-ge-custom/releases/download/GE-Proton8-26/wine-lutris-GE-Proton8-26-x86_64.tar.xz'

proton='https://github.com/Kron4ek/Wine-Builds/releases/download/proton-8.0-5/wine-proton-8.0-5-amd64.tar.xz'

wine='https://github.com/Kron4ek/Wine-Builds/releases/download/9.2/wine-9.2-staging-tkg-amd64.tar.xz'

clear

echo "#################################################################"
echo "(1)> (Install) >> (the wine-ge-custom created by GloriousEggroll version of Arch-Linux)"
echo "(2)> (Install) >> (the proton by Kron4ek version of Arch-Linux)"
echo "(3)> (Install) >> (the wine-staging-tkg by Kron4ek version of Arch-Linux)"
echo "(4)> (Install) >> (the wine-staging-tkg by Kron4ek version of Void)"
echo "(5)> (Update) >> (The custom wine with Winetricks)"
echo "(6)> (Exit)"
echo "#################################################################"

read -p "(Enter your choice) >> " choice
echo "#################################################################"

case $choice in
1)
show_arch-with-wine-ge-custom
;;
2)
show_arch-with-proton
;;
3)
show_arch-with-wine
;;
4)
show_void-with-wine
;;
5)
show_update-custom-wine-with-winetricks
;;
6)
exit 0
;;
*)
echo "(Invalid choice. Please try again)"
echo "#################################################################"
sleep 2
;;
esac
done
}

show_arch-with-wine-ge-custom() {
while true; do
clear
if [ ! -x /bin/pacman ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
read -p "(This action may have unintended consequences. Are you sure you want to continue) (y/n) >> " second_confirm
echo "#################################################################"
if [[ "$second_confirm" == "y" || "$second_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script is provided 'AS IS', without any warranties of any kind. The user assumes full responsibility for executing this script and any resulting consequences. We recommend backing up your data before proceeding. If the script does not cause any apparent issues, you can use the PC normally. Are you sure you want to proceed) (y/n) >> " third_confirm
echo "#################################################################"
if [[ "$third_confirm" == "y" || "$third_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script removes the currently installed Wine and some Wine-related utilities to enable the installation of wine-ge-custom. If you're wondering which utilities will be removed >> wine-mono, wine-gecko, winetricks. During the installation of wine-ge-custom, you will be prompted to choose whether or not you want to install winetricks. Winetricks will be installed from the Winetricks GitHub repository instead of the distribution's repository to avoid potential issues. Do not install wine-mono, wine-gecko, winetricks, wine-staging, and wine from the distribution's repository, as installing these components without uninstalling wine-ge-custom may result in system issues. If you uninstall wine-ge-custom, you can install them without any problems) (y/n) >> " four_confirm
echo "#################################################################"
if [[ "$four_confirm" == "y" || "$four_confirm" == "Y" ]]; then
echo "(Proceeding with the changes)"
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Arch)" 
echo "#################################################################"
sudo pacman -R wine wine-mono wine-gecko winetricks
sudo pacman -R wine-staging
sudo pacman -Sy
sudo pacman -S lib32-gamemode lib32-acl lib32-attr lib32-fontconfig lib32-freetype2 lib32-gettext lib32-glib2 lib32-harfbuzz lib32-libnl lib32-libpcap lib32-libpng lib32-libxcursor lib32-libxrandr lib32-libxrender lib32-pcre2 lib32-util-linux gamemode cabextract xorg-xmessage tar lib32-vkd3d vkd3d lib32-libxkbcommon -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pacman -Sy
sudo pacman -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

wget $wine-ge

tar -xvf wine-lutris-GE-Proton8-26-x86_64.tar.xz

cd lutris-GE-Proton8-26-x86_64

clear

echo "#################################################################"
echo "(# /bin/ #)"
echo "#################################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/msidb

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/regedit

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#################################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/wine*

echo "(# Installing Wine #)"
echo "#################################################################"

sudo cp -r bin/* /usr/bin

sudo mkdir /usr/share/wine

sudo mkdir /usr/lib32/wine

sudo mkdir /usr/lib64/wine

sudo mkdir /lib/wine

sudo mkdir /lib64/wine

sudo cp -r lib/wine/* /usr/lib32/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib64/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r lib64/wine/* /lib/wine/

sudo cp -r lib64/wine/* /lib64/wine/

sudo cp -r share/wine/* /usr/share/wine

cd ..

sudo rm -rf lutris-GE-Proton8-26-x86_64

sudo rm -rf wine-lutris-GE-Proton8-26-x86_64.tar.xz

echo "#################################################################"

clear

sudo rm /usr/bin/wine-ge-custom-version

sudo rm /usr/bin/wine-xwayland

sudo rm /usr/bin/custom-wine-with-winetricks

sudo rm /usr/bin/dxvk-vkd3d.reg

clear

sudo cp wine-ge-custom-version /usr/bin/

sudo chmod +x /usr/bin/wine-ge-custom-version

sudo cp wine-xwayland /usr/bin/

sudo chmod +x /usr/bin/wine-xwayland

sudo cp custom-wine-with-winetricks /usr/bin/

sudo chmod +x /usr/bin/custom-wine-with-winetricks

sudo cp dxvk-vkd3d.reg /usr/bin/

clear

echo "#################################################################"

clear

sudo rm -rf /usr/bin/winetricks

wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks

sudo cp winetricks /usr/bin/

sudo chmod +x /usr/bin/winetricks

sudo rm -rf winetricks

clear

sudo cp -r FSYNC-ON-WINE-GE-CUSTOM/* /usr/share/applications

clear

sudo pacman -Sy

sudo pacman -S lib32-vkd3d vkd3d

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(The wine-ge-custom has been successfully installed; all previously installed Wine programs in the distribution have been replaced with wine-ge-custom. To check the version, use the command >> wine-ge-custom-version. If you have it installed, remove the /home/your_user/.wine folder to avoid conflicts with prefixes from other Wine versions. To uninstall, use the command >> chmod a+x uninstall.sh; after executing chmod a+x uninstall.sh, use the command ./uninstall.sh. To enable support for DXVK/VKD3D and install some DLLs to run Windows games or programs, use the custom-wine-with-winetricks)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_arch-with-proton() {
while true; do
clear
if [ ! -x /bin/pacman ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
read -p "(This action may have unintended consequences. Are you sure you want to continue) (y/n) >> " second_confirm
echo "#################################################################"
if [[ "$second_confirm" == "y" || "$second_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script is provided 'AS IS', without any warranties of any kind. The user assumes full responsibility for executing this script and any resulting consequences. We recommend backing up your data before proceeding. If the script does not cause any apparent issues, you can use the PC normally. Are you sure you want to proceed) (y/n) >> " third_confirm
echo "#################################################################"
if [[ "$third_confirm" == "y" || "$third_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script removes the currently installed Wine and some Wine-related utilities to enable the installation of proton. If you're wondering which utilities will be removed >> wine-mono, wine-gecko, winetricks. During the installation of proton, you will be prompted to choose whether or not you want to install winetricks. Winetricks will be installed from the Winetricks GitHub repository instead of the distribution's repository to avoid potential issues. Do not install wine-mono, wine-gecko, winetricks, wine-staging, and wine from the distribution's repository, as installing these components without uninstalling proton may result in system issues. If you uninstall proton, you can install them without any problems) (y/n) >> " four_confirm
echo "#################################################################"
if [[ "$four_confirm" == "y" || "$four_confirm" == "Y" ]]; then
echo "(Proceeding with the changes)"
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Arch)" 
echo "#################################################################"
sudo pacman -R wine wine-mono wine-gecko winetricks
sudo pacman -R wine-staging
sudo pacman -Sy
sudo pacman -S lib32-gamemode lib32-acl lib32-attr lib32-fontconfig lib32-freetype2 lib32-gettext lib32-glib2 lib32-harfbuzz lib32-libnl lib32-libpcap lib32-libpng lib32-libxcursor lib32-libxrandr lib32-libxrender lib32-pcre2 lib32-util-linux gamemode cabextract xorg-xmessage tar lib32-vkd3d vkd3d lib32-libxkbcommon -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pacman -Sy
sudo pacman -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

wget $proton

tar -xvf wine-proton-8.0-5-amd64.tar.xz

cd wine-proton-8.0-5-amd64

clear

echo "#################################################################"
echo "(# /bin/ #)"
echo "#################################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/function_grep.pl

sudo rm -rf /bin/msidb

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regedit

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/widl

sudo rm -rf /bin/wmc

sudo rm -rf /bin/wrc

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#################################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/function_grep.pl

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/widl

sudo rm -rf /usr/bin/wmc

sudo rm -rf /usr/bin/wrc

sudo rm -rf /usr/bin/wine*

echo "(# Installing Wine #)"
echo "#################################################################"

sudo cp -r bin/* /usr/bin

sudo mkdir /usr/share/wine

sudo mkdir /usr/lib32/wine

sudo mkdir /usr/lib64/wine

sudo mkdir /lib/wine

sudo mkdir /lib64/wine

sudo cp -r lib/wine/* /usr/lib32/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r share/wine/* /usr/share/wine

cd ..

sudo rm -rf wine-proton-8.0-5-amd64

sudo rm -rf wine-proton-8.0-5-amd64.tar.xz

echo "#################################################################"

clear

sudo rm /usr/bin/proton-version

sudo rm /usr/bin/wine-xwayland

sudo rm /usr/bin/custom-wine-with-winetricks

sudo rm /usr/bin/dxvk-vkd3d.reg

clear

sudo cp proton-version /usr/bin/

sudo chmod +x /usr/bin/proton-version

sudo cp wine-xwayland /usr/bin/

sudo chmod +x /usr/bin/wine-xwayland

sudo cp custom-wine-with-winetricks /usr/bin/

sudo chmod +x /usr/bin/custom-wine-with-winetricks

sudo cp dxvk-vkd3d.reg /usr/bin/

clear

echo "#################################################################"

clear

sudo rm -rf /usr/bin/winetricks

wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks

sudo cp winetricks /usr/bin/

sudo chmod +x /usr/bin/winetricks

sudo rm -rf winetricks

clear

sudo cp -r FSYNC-ON-WINE-GE-CUSTOM/* /usr/share/applications

clear

sudo pacman -Sy

sudo pacman -S lib32-vkd3d vkd3d

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(The proton has been successfully installed; all previously installed Wine programs in the distribution have been replaced with proton. To check the version, use the command >> proton-version. If you have it installed, remove the /home/your_user/.wine folder to avoid conflicts with prefixes from other Wine versions. To uninstall, use the command >> chmod a+x uninstall.sh; after executing chmod a+x uninstall.sh, use the command ./uninstall.sh. To enable support for DXVK/VKD3D and install some DLLs to run Windows games or programs, use the custom-wine-with-winetricks)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_arch-with-wine() {
while true; do
clear
if [ ! -x /bin/pacman ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
read -p "(This action may have unintended consequences. Are you sure you want to continue) (y/n) >> " second_confirm
echo "#################################################################"
if [[ "$second_confirm" == "y" || "$second_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script is provided 'AS IS', without any warranties of any kind. The user assumes full responsibility for executing this script and any resulting consequences. We recommend backing up your data before proceeding. If the script does not cause any apparent issues, you can use the PC normally. Are you sure you want to proceed) (y/n) >> " third_confirm
echo "#################################################################"
if [[ "$third_confirm" == "y" || "$third_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script removes the currently installed Wine and some Wine-related utilities to enable the installation of wine. If you're wondering which utilities will be removed >> wine-mono, wine-gecko, winetricks. During the installation of wine, you will be prompted to choose whether or not you want to install winetricks. Winetricks will be installed from the Winetricks GitHub repository instead of the distribution's repository to avoid potential issues. Do not install wine-mono, wine-gecko, winetricks, wine-staging, and wine from the distribution's repository, as installing these components without uninstalling wine may result in system issues. If you uninstall wine, you can install them without any problems) (y/n) >> " four_confirm
echo "#################################################################"
if [[ "$four_confirm" == "y" || "$four_confirm" == "Y" ]]; then
echo "(Proceeding with the changes)"
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Arch)" 
echo "#################################################################"
sudo pacman -R wine wine-mono wine-gecko winetricks
sudo pacman -R wine-staging
sudo pacman -Sy
sudo pacman -S lib32-gamemode lib32-acl lib32-attr lib32-fontconfig lib32-freetype2 lib32-gettext lib32-glib2 lib32-harfbuzz lib32-libnl lib32-libpcap lib32-libpng lib32-libxcursor lib32-libxrandr lib32-libxrender lib32-pcre2 lib32-util-linux gamemode cabextract xorg-xmessage tar lib32-vkd3d vkd3d lib32-libxkbcommon -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pacman -Sy
sudo pacman -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

wget $wine

tar -xvf wine-9.2-staging-tkg-amd64.tar.xz

cd wine-9.2-staging-tkg-amd64

clear

echo "#################################################################"
echo "(# /bin/ #)"
echo "#################################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/function_grep.pl

sudo rm -rf /bin/msidb

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regedit

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/widl

sudo rm -rf /bin/wmc

sudo rm -rf /bin/wrc

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#################################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/function_grep.pl

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/widl

sudo rm -rf /usr/bin/wmc

sudo rm -rf /usr/bin/wrc

sudo rm -rf /usr/bin/wine*

echo "(# Installing Wine #)"
echo "#################################################################"

sudo cp -r bin/* /usr/bin

sudo mkdir /usr/share/wine

sudo mkdir /usr/lib32/wine

sudo mkdir /usr/lib64/wine

sudo mkdir /lib/wine

sudo mkdir /lib64/wine

sudo cp -r lib/wine/* /usr/lib32/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r share/wine/* /usr/share/wine

cd ..

sudo rm -rf wine-9.2-staging-tkg-amd64

sudo rm -rf wine-9.2-staging-tkg-amd64.tar.xz

echo "#################################################################"

clear

sudo rm /usr/bin/wine-version

sudo rm /usr/bin/wine-xwayland

sudo rm /usr/bin/custom-wine-with-winetricks

sudo rm /usr/bin/dxvk-vkd3d.reg

clear

sudo cp wine-version /usr/bin/

sudo chmod +x /usr/bin/wine-version

sudo cp wine-xwayland /usr/bin/

sudo chmod +x /usr/bin/wine-xwayland

sudo cp custom-wine-with-winetricks /usr/bin/

sudo chmod +x /usr/bin/custom-wine-with-winetricks

sudo cp dxvk-vkd3d.reg /usr/bin/

clear

echo "#################################################################"

clear

sudo rm -rf /usr/bin/winetricks

wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks

sudo cp winetricks /usr/bin/

sudo chmod +x /usr/bin/winetricks

sudo rm -rf winetricks

clear

sudo cp -r FSYNC-ON-WINE-GE-CUSTOM/* /usr/share/applications

clear

sudo pacman -Sy

sudo pacman -S lib32-vkd3d vkd3d

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(The wine has been successfully installed; all previously installed Wine programs in the distribution have been replaced with wine To check the version, use the command >> wine-custom-version. If you have it installed, remove the /home/your_user/.wine folder to avoid conflicts with prefixes from other Wine versions. To uninstall, use the command >> chmod a+x uninstall.sh; after executing chmod a+x uninstall.sh, use the command ./uninstall.sh. To enable support for DXVK/VKD3D and install some DLLs to run Windows games or programs, use the custom-wine-with-winetricks)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_void-with-wine() {
while true; do
clear
if [ ! -x /bin/xbps-install ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
read -p "(This action may have unintended consequences. Are you sure you want to continue) (y/n) >> " second_confirm
echo "#################################################################"
if [[ "$second_confirm" == "y" || "$second_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script is provided 'AS IS', without any warranties of any kind. The user assumes full responsibility for executing this script and any resulting consequences. We recommend backing up your data before proceeding. If the script does not cause any apparent issues, you can use the PC normally. Are you sure you want to proceed) (y/n) >> " third_confirm
echo "#################################################################"
if [[ "$third_confirm" == "y" || "$third_confirm" == "Y" ]]; then
read -p "(Warning) >> (This script removes the currently installed Wine and some Wine-related utilities to enable the installation of wine. If you're wondering which utilities will be removed >> wine-mono, wine-gecko, winetricks. During the installation of wine, you will be prompted to choose whether or not you want to install winetricks. Winetricks will be installed from the Winetricks GitHub repository instead of the distribution's repository to avoid potential issues. Do not install wine-mono, wine-gecko, winetricks, wine-staging, and wine from the distribution's repository, as installing these components without uninstalling wine may result in system issues. If you uninstall wine, you can install them without any problems) (y/n) >> " four_confirm
echo "#################################################################"
if [[ "$four_confirm" == "y" || "$four_confirm" == "Y" ]]; then
echo "(Proceeding with the changes)"
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
else
echo "(Action canceled by the user)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Void)" 
echo "#################################################################"
sudo xbps-remove -R wine wine-mono wine-gecko winetricks
sudo xbps-remove -R wine-staging
sudo xbps-install -Sy
sudo xbps-install -S libgamemode-32bit acl-32bit attr-32bit fontconfig-32bit freetype-32bit gettext-32bit glib-32bit libharfbuzz-32bit libnl-32bit libpcap-32bit libpng-32bit libXcursor-32bit libXrandr-32bit libXrender-32bit libpcre2-32bit gamemode cabextract xmessage tar libOSMesa libxkbregistry libxkbcommon-32bit libOSMesa-32bit libxkbregistry-32bit -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo xbps-install -Sy
sudo xbps-install -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

wget $wine

tar -xvf wine-9.2-staging-tkg-amd64.tar.xz

cd wine-9.2-staging-tkg-amd64

clear

echo "#################################################################"
echo "(# /bin/ #)"
echo "#################################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/function_grep.pl

sudo rm -rf /bin/msidb

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regedit

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/widl

sudo rm -rf /bin/wmc

sudo rm -rf /bin/wrc

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#################################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/function_grep.pl

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/widl

sudo rm -rf /usr/bin/wmc

sudo rm -rf /usr/bin/wrc

sudo rm -rf /usr/bin/wine*

echo "(# Installing Wine #)"
echo "#################################################################"

sudo cp -r bin/* /usr/bin

sudo mkdir /usr/share/wine

sudo mkdir /usr/lib32/wine

sudo mkdir /usr/lib64/wine

sudo mkdir /lib/wine

sudo mkdir /lib64/wine

sudo cp -r lib/wine/* /usr/lib32/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /usr/lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r lib/wine/* /lib/wine/

sudo cp -r lib/wine/* /lib64/wine/

sudo cp -r share/wine/* /usr/share/wine

cd ..

sudo rm -rf wine-9.2-staging-tkg-amd64

sudo rm -rf wine-9.2-staging-tkg-amd64.tar.xz

echo "#################################################################"

clear

sudo rm /usr/bin/wine-version

sudo rm /usr/bin/wine-xwayland

sudo rm /usr/bin/custom-wine-with-winetricks

sudo rm /usr/bin/dxvk-vkd3d.reg

clear

sudo cp wine-version /usr/bin/

sudo chmod +x /usr/bin/wine-version

sudo cp wine-xwayland /usr/bin/

sudo chmod +x /usr/bin/wine-xwayland

sudo cp custom-wine-with-winetricks /usr/bin/

sudo chmod +x /usr/bin/custom-wine-with-winetricks

sudo cp dxvk-vkd3d.reg /usr/bin/

clear

echo "#################################################################"

clear

sudo rm -rf /usr/bin/winetricks

wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks

sudo cp winetricks /usr/bin/

sudo chmod +x /usr/bin/winetricks

sudo rm -rf winetricks

clear

sudo cp -r FSYNC-ON-WINE-GE-CUSTOM/* /usr/share/applications

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(The wine has been successfully installed; all previously installed Wine programs in the distribution have been replaced with wine To check the version, use the command >> wine-custom-version. If you have it installed, remove the /home/your_user/.wine folder to avoid conflicts with prefixes from other Wine versions. To uninstall, use the command >> chmod a+x uninstall.sh; after executing chmod a+x uninstall.sh, use the command ./uninstall.sh. To enable support for DXVK/VKD3D and install some DLLs to run Windows games or programs, use the custom-wine-with-winetricks)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_update-custom-wine-with-winetricks() {
while true; do
clear

clear

sudo rm /usr/bin/custom-wine-with-winetricks

sudo cp custom-wine-with-winetricks /usr/bin/ 

sudo chmod +x /usr/bin/custom-wine-with-winetricks

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Update completed)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_main_menu
