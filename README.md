# CUSTOM-WINE-INSTALLER

- It will no longer be maintained. use 'Bottles' to run Windows programs/games.

- NOTE: If you update wine-ge-custom/proton/wine, run custom-wine-with-winetricks to avoid problem

####################################

# wine-ge-custom

- wine-ge-custom-version: feb 2024

- build-latest: Wine-GE-Proton8-26

####################################

# proton

- proton-version: jan 2024

- build-latest: Proton 8.0-5

####################################

# wine

- wine-version: feb 2024

- build-latest: Wine 9.2

####################################

# custom-wine-with-winetricks

- build-latest: 0.0.6

####################################

- Support for the distro: Arch-Linux/Void-Linux

- Use at your own risk

- Credits to GloriousEggroll from wine-ge-custom, and credits to Winetricks, and credits to Kron4ek from Wine-Builds, with links to their GitHub profiles: GloriousEggroll's profile is at https://github.com/GloriousEggroll/, Kron4ek's profile is at https://github.com/Kron4ek/, Winetricks' profile is at https://github.com/Winetricks/. Also, credits to https://github.com/doitsujin, from dxvk in custom-wine-with-winetricks, and https://github.com/HansKristian-Work from vkd3d in custom-wine-with-winetricks

- CUSTOM-WINE-INSTALLER is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with CUSTOM-WINE-INSTALLER, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of CUSTOM-WINE-INSTALLER to add additional features.

## Installation

- To install CUSTOM-WINE-INSTALLER, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/custom-wine-installer.git

# 2. To install the CUSTOM-WINE-INSTALLER script, follow these steps

- chmod a+x `installupdate.sh`

- `./installupdate.sh`

# To use custom-wine-with-winetricks

`custom-wine-with-winetricks`

# For check version

####################################

# For wine-ge-custom

sudo `wine-ge-custom-version` or `wine-ge-custom-version`

####################################

# For proton

sudo `proton-version` or `proton-version`

####################################

# For wine

sudo `wine-version` or `wine-version`

####################################

# For uninstall

- chmod a+x `uninstall.sh`

- `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# License

- CUSTOM-WINE-INSTALLER is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of CUSTOM-WINE-INSTALLER.
