#!/bin/bash

clear
echo "#############################################################"
echo "(custom-wine-uninstaller) >> (feb 2024)"
echo "#############################################################"
echo "██     ██ ██ ███    ██ ███████ "
echo "██     ██ ██ ████   ██ ██      "
echo "██  █  ██ ██ ██ ██  ██ █████   "
echo "██ ███ ██ ██ ██  ██ ██ ██      "
echo " ███ ███  ██ ██   ████ ███████ "
echo "#############################################################"
echo "(custom-wine-uninstaller-gitlab) >> (https://gitlab.com/manoel-linux1/custom-wine-installer)"
echo "#############################################################"

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#############################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#############################################################"
exit 1
fi

clear

echo "#############################################################"
echo "wine-ge-custom"
echo "#############################################################"

clear

echo "#############################################################"
echo "(# /bin/ #)"
echo "#############################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/msidb

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/regedit

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#############################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/wine*

clear

sudo rm -rf /usr/bin/wine-ge-custom-version

sudo rm -rf /usr/bin/wine-xwayland

sudo rm -rf /usr/bin/winetricks

sudo rm -rf /usr/bin/custom-wine-with-winetricks

sudo rm -rf /usr/bin/dxvk-vkd3d.reg

clear

echo "#############################################################"
echo "proton"
echo "#############################################################"

clear

echo "#############################################################"
echo "(# /bin/ #)"
echo "#############################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/function_grep.pl

sudo rm -rf /bin/msidb

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regedit

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/widl

sudo rm -rf /bin/wmc

sudo rm -rf /bin/wrc

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#############################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/function_grep.pl

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/widl

sudo rm -rf /usr/bin/wmc

sudo rm -rf /usr/bin/wrc

sudo rm -rf /usr/bin/wine*

clear

sudo rm -rf /usr/bin/proton-version

sudo rm -rf /usr/bin/wine-xwayland

sudo rm -rf /usr/bin/winetricks

sudo rm -rf /usr/bin/custom-wine-with-winetricks

sudo rm -rf /usr/bin/dxvk-vkd3d.reg

clear

echo "#############################################################"
echo "wine"
echo "#############################################################"

clear

echo "#############################################################"
echo "(# /bin/ #)"
echo "#############################################################"

sudo rm -rf /lib/wine/*

sudo rm -rf /lib64/wine/*

sudo rm -rf /lib/vkd3d

sudo rm -rf /lib64/vkd3d

sudo rm -rf /bin/function_grep.pl

sudo rm -rf /bin/msidb

sudo rm -rf /bin/msiexec

sudo rm -rf /bin/notepad

sudo rm -rf /bin/regedit

sudo rm -rf /bin/regsvr32

sudo rm -rf /bin/widl

sudo rm -rf /bin/wmc

sudo rm -rf /bin/wrc

sudo rm -rf /bin/wine*

echo "(# /usr/bin/ #)"
echo "#############################################################"

sudo rm -rf /usr/lib/wine/*

sudo rm -rf /usr/lib32/wine/*

sudo rm -rf /usr/lib64/wine/*

sudo rm -rf /usr/lib/vkd3d

sudo rm -rf /usr/lib32/vkd3d

sudo rm -rf /usr/lib64/vkd3d

sudo rm -rf /usr/share/wine/*

sudo rm -rf /usr/share/applications/wine.desktop

sudo rm -rf /usr/share/applications/xwayland-wine.desktop

sudo rm -rf /usr/bin/function_grep.pl

sudo rm -rf /usr/bin/msidb

sudo rm -rf /usr/bin/msiexec

sudo rm -rf /usr/bin/notepad

sudo rm -rf /usr/bin/regedit

sudo rm -rf /usr/bin/regsvr32

sudo rm -rf /usr/bin/widl

sudo rm -rf /usr/bin/wmc

sudo rm -rf /usr/bin/wrc

sudo rm -rf /usr/bin/wine*

clear

sudo rm -rf /usr/bin/wine-version

sudo rm -rf /usr/bin/wine-xwayland

sudo rm -rf /usr/bin/winetricks

sudo rm -rf /usr/bin/custom-wine-with-winetricks

sudo rm -rf /usr/bin/dxvk-vkd3d.reg

clear

sudo pacman -R lib32-vkd3d vkd3d

clear

echo "#############################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ " 
echo "#############################################################"
echo "(Uninstallation completed)"
echo "#############################################################"
